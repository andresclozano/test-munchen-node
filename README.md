# Backend test for Munchen

This project was developed for test for Munchen by Andrés Lozano as Backend Developer

# Setup

Configure a validat postgres database in .env in root folder

    DATABASE_URL="postgresql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DATABASE}?schema=public"

Run this command to migrate prisma

```
$ prisma migrate dev --name init
```

Run this commands to initialize the project

```
$ npm init
$ npx prisma migrate dev --name init
$ sls offline
```

# API

### List Users
`GET /dev/user`


### Create User
`POST /dev/user`

    {"name" : "andres", "email": "andres@gmail.com", "balance": 1500}

### Increase Balance User
`PUT /dev/user/balance/:userId/:balance`

### Transfer balance between users
`PUT /dev/user/transfer/:sourceUserId/:destinationUserId/:balance`

### List Products
`GET /dev/product`

### Create Product
`POST /dev/product`

    {"name" : "pepsi", "price" : "500"}

### Create Order
`POST /dev/order`

    {"userId" : 1, "details" : [{ "productId" : 1, "amount" : 2}]}