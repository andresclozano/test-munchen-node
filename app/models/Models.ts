export interface User {
    id: number
    name: string
    email: string
    balance: number
}

export interface Product {
    id: number
    name: string
    price: number
}

export interface PurchaseOrder {
    id: number
    userId: number
    date: Date
    total: number
    details: PurchaseOrderDetail[]
}

export interface PurchaseOrderDetail {
    id: number
    purchaseOrderId: number
    productId: number
    amount: number
}