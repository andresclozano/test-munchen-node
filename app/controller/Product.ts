import { PrismaClient } from "@prisma/client";
import { Product } from "../models/Models";

const prisma = new PrismaClient();

export class ProductController {

    async createProduct(event: any) {
        const product: Product = JSON.parse(event.body)

        try {
            const insert = await prisma.products.create({ data: product})
            if (insert) {
                return {
                    statusCode: 200,
                    body: JSON.stringify(insert)
                }
            }
        } catch (err) {
            console.log('Error ProductController.createProduct: ', err)
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message : 'Error at insert product',
                    err
                })
            }
        }
    }

    async getProducts() {
        try {
            const products = await prisma.products.findMany();
            return {
                statusCode: 200,
                body: JSON.stringify(products) 
            }
        } catch (err) {
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message : 'Error at list users',
                    err,
                    database: process.env.DATABASE_URL
                })
            }
        }
    }

}