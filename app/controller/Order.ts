import { PrismaClient } from "@prisma/client";
import { PurchaseOrder, PurchaseOrderDetail } from "../models/Models";

const prisma = new PrismaClient();

export class OrderController {

    async createOrder(event: any) {

        const order: PurchaseOrder = JSON.parse(event.body)

        try {
            const total = await this.getTotal(order.details);

            const insertedOrder: PurchaseOrder = await prisma.purchase_order.create({
                data: {
                    userId : order.userId,
                    total : total
                }
            })


            if (insertedOrder) {
                const detailOrder = order.details.map((detail: PurchaseOrderDetail) => {
                    detail.purchaseOrderId = insertedOrder.id
                    return detail
                })

                const insertedDetail = await prisma.purchase_order_detail.createMany({
                    data: detailOrder
                });

                if (insertedDetail) {

                    const discountUserBalance = await this.discountUser(order.userId, total)
                    if (discountUserBalance) {
                        return { 
                            statusCode: 200,
                            body: JSON.stringify({
                                message : 'Purchase order added successfully'
                            })
                        }
                    } else {
                        return {
                            statusCode: 500,
                            body: JSON.stringify({
                                message: 'Error at Discount User Balance'
                            })
                        }
                    }
                } else {
                    return {
                        statusCode: 500,
                        body: JSON.stringify({
                            message: 'Error at Add Order Detail'
                        })
                    }
                }
            } else {
                return {
                    statusCode: 500,
                    body: JSON.stringify({
                        message: 'Error at Add Order'
                    })
                }
            }            
        } catch (error) {
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message: 'Error at Add Purchase Order'
                })
            }
        }
    }

    async getTotal (detail: PurchaseOrderDetail[]) {
        let total = 0

        if (detail && detail.length > 0) {
            for(let i = 0; i < detail.length; i++) {
                const d: PurchaseOrderDetail = detail[i]
                const product = await prisma.products.findUnique({ where: { id : d.productId } })
                if (product) {
                    total += product.price * d.amount
                }
            }
        }

        return total;
    }

    async discountUser (userId: number, total: number): Promise<boolean> {
        const user = await prisma.users.findUnique({
            where: {
                id : userId
            }
        })

        if (user) {
            user.balance = user.balance - total
            const updateUser  = await prisma.users.update({
                where: { 
                    id: userId 
                },
                data: { 
                    balance : user.balance 
                }
            })

            return updateUser ? true : false
        }

        return false;
    }
}