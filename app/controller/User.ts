import { PrismaClient } from "@prisma/client"
import { User } from "../models/Models"

const prisma = new PrismaClient();

export class UserController {

    async createUser(event: any) {

        const user: User = JSON.parse(event.body)

        try{
            const insert = await prisma.users.create({ data: user})
            if (insert) {
                return {
                    statusCode: 200,
                    body: JSON.stringify(insert)
                }
            }
        } catch(err){
            console.log('Error UserController.createUser: ', err)
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message : 'Error at insert user'
                })
            }
        }
    }

    async getUsers() {
        try {
            const users = await prisma.users.findMany()
            return {
                statusCode: 200,
                body: JSON.stringify(users)
            }
        } catch (err) {
            console.error('Error UserController.getUsers(): ', err)
            return {
                statusCode: 500,
                body: JSON.stringify({
                    message : 'Error at list users',
                    err
                })
            }
        }
    }

    async addBalanceUser(event: any) {
        const id = Number(event.pathParameters.userId)
        const balance = Number(event.pathParameters.balance)

        try {
            const user = await prisma.users.findUnique({where: { id : id }})

            if (user) {
                user.balance = user.balance + balance
                const updateUser  = await prisma.users.update({where: { id: id },data: { balance : user.balance }})

                return { statusCode: 200, body: JSON.stringify(updateUser)}
            } else {
                return {statusCode: 404, body: JSON.stringify({ message: 'User not found'})}
            }
            
        }catch (err) {
            console.error('Error UserController.addBalanceUser:', err)
            return {statusCode: 500, body: JSON.stringify({ message: 'Error at Add Balance User'})}
        }
    }

    async transferBalanceUsers(event: any) {
        const sourceUserId = Number(event.pathParameters.sourceUserId);
        const destinarionUserId = Number(event.pathParameters.destinarionUserId);
        const balance = Number(event.pathParameters.balance);

        try {
            const sourceUser = await prisma.users.findUnique({where: { id : sourceUserId }})
            const destinarionUser = await prisma.users.findUnique({where: { id : destinarionUserId}})

            if (sourceUser && destinarionUser) {
                sourceUser.balance = sourceUser.balance - balance
                destinarionUser.balance = destinarionUser.balance + balance

                const updateSource  = await prisma.users.update({where: { id: sourceUserId },data: { balance : sourceUser.balance }})
                const updateDestination = await prisma.users.update({where: { id: destinarionUserId },data: { balance : destinarionUser.balance }})
                return { statusCode: 200, body: JSON.stringify({message : 'Balance transfered successfully'})}

            } else {
                return {statusCode: 404, body: JSON.stringify({ message: 'User source or destination not found'})}
            }
        } catch (err) {
            return {statusCode: 500, body: JSON.stringify({ message: 'Error at Transfer Balance User', err})}
        }
    }

}
