import { ProductController } from "../controller/Product"

const productController = new ProductController()

export const createProduct = (event: any) => {
    return productController.createProduct(event)
}

export const getProducts = () => {
    return productController.getProducts()
}