import { OrderController } from "../controller/Order";

const orderController = new OrderController();

export const createOrder = (event: any) => {
    return orderController.createOrder(event)
}