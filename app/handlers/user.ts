import { UserController } from "../controller/User";

const userController = new UserController()

export const createUser = (event: any) => {
    return userController.createUser(event);
}

export const getUsers = () => {
    return userController.getUsers()
}

export const addBalanceUser = (event: any) => {
    return userController.addBalanceUser(event)
}

export const transferBalanceUser = (event: any) => {
    return userController.transferBalanceUsers(event)
}